## Overview

A docker image for wordpress plugin development

* https://hub.docker.com/r/borgogelli/hello-docker-wp

## Notes

That Dockerfile uses the official WordPress image and adds the [wp-cli tool](https://wp-cli.org).

Note that, there is also an official an official WordPress Docker images variant that include wp-cli but ***does not*** contain WordPress itself.
Therefore this image is designed to be consumed via the docker-compose tool (see the [official wordpress image](https://hub.docker.com/_/wordpress/) and [samples](https://docs.docker.com/samples/library/wordpress/)).

## Build

### Automatically 

That image is builded automatically by the Bitbucket pipeline on every commit (push strategy)

* see https://bitbucket.org/borgogelli/hello-docker-wp/addon/pipelines/home

It is also possible to configure the automatic compilation feature on cloud.docker.com (pull strategy)

* see https://cloud.docker.com/repository/docker/borgogelli/hello-docker-wp/builds

### Manually

To manually build the image from your docker server:

```
sudo docker login --username=borgogelli
sudo docker build --tag borgogelli/hello-docker-wp:latest .
sudo docker push borgogelli/hello-docker-wp:latest
```

...or more briefly:

```
npm docker
```