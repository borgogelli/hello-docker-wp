FROM wordpress:5.2-php7.3-apache
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y apt-utils

# Add sudo too, in order to run wp-cli as the www-data user 
RUN apt-get install -y sudo wget default-mysql-client

# Add WP-CLI 
RUN curl -o /bin/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
COPY wp-su.sh /bin/wp
RUN chmod +x /bin/wp-cli.phar /bin/wp

# NodeJs
RUN apt-get install -y nodejs npm
RUN npm install -g npm@>=6.0.0
RUN node -v
RUN npm -v

# Php tools
RUN pwd
RUN whoami
RUN echo "Installing composer..."
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer -V
RUN echo "Installing phpunit..."
RUN wget --no-verbose --output-document=/tmp/phpunit https://phar.phpunit.de/phpunit-7.phar
# RUN curl -o /tmp/phpunit https://phar.phpunit.de/phpunit-7.phar
RUN chmod +x /tmp/phpunit && mv /tmp/phpunit /usr/local/bin/phpunit
RUN echo "Installing codeception..."
RUN wget --no-verbose --output-document=/tmp/codecept http://codeception.com/codecept.phar
RUN chmod +x /tmp/codecept && mv /tmp/codecept /usr/local/bin/codecept

# Cleanup
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*



